const responseHandler = require('responseHandler')
const AWS = require('aws-sdk')
const DynamoDB = new AWS.DynamoDB.DocumentClient()
const cisp = new AWS.CognitoIdentityServiceProvider();


const TableName = process.env.TABLE_NAME



exports.handler = async (event) => {

    try {
        const accessToken = event.queryStringParameters.accessToken
        console.log({ accessToken })
        let items;
        console.log({ event })
        const type = event.pathParameters.type
        console.log({ type })
        if (type === 'all') {
            let params = { TableName }
            items = await DynamoDB.scan(params).promise()

        } else if (type === 'single') {
            console.log("entered")

            const cispParams = {
                "AccessToken": accessToken
            };

            const result = await cisp.getUser(cispParams).promise()
            const userId = result.userAttributes[0].Value;
            console.log({ userId })

            let params = {
                TableName,
                Key: {
                    UserId: userId
                }
            }
            items = await DynamoDB.get(params).promise()
        }

        if (items) {
            return responseHandler.responseStatus(200, {
                message: 'Successfully fetched',
                data: items
            })
        } else {
            throw "enter valid params in url."
        }

    } catch (err) {
        return err
    }
}