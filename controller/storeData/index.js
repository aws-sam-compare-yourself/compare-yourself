const responseHandler = require('responseHandler')
const AWS = require('aws-sdk')
const DynamoDB = new AWS.DynamoDB.DocumentClient()

const TableName = process.env.TABLE_NAME


module.exports.handler = async (event) => {
    console.log({ event })
    console.log({ eventauth: event.requestContext.authorizer })
    try {
        let data = JSON.parse(event.body)

        if (data.Age && data.Height && data.Income) {
            console.log({ data })
            let items = {
                UserId: event.requestContext.authorizer.claims.sub,
                Age: data.Age,
                Height: data.Height,
                Income: data.Income
            }
            await DynamoDB.put({
                TableName,
                Item: items
            }).promise()

            console.log({
                response: responseHandler.responseStatus(200, {
                    message: "Added user"
                })
            })

            return responseHandler.responseStatus(200, {
                message: "Added user"
            })
        } else {
            throw 'Input must contain Age, Height, Income values.!'
        }


    } catch (err) {
        return err
    }
}