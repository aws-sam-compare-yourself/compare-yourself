// Lamda for custom authorization
// https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-use-lambda-authorizer.html

exports.handler = async (event) => {
    console.log({ event })
    try {
        const token = event.authorizationToken;
        if (token === 'allow') {
            console.log('entered')
            const policy = genPolicy("usernevinedwin", 'Allow', event.methodArn)
            return policy
        } else if (token === 'deny') {
            return genPolicy("usernevinedwin", 'Deny', event.methodArn)
        } else {
            return 'unauthorized'
        }
    } catch (err) {
        return err
    }
}


// function for generating custom policy
const genPolicy = (principalId, Effect, Resource) => {

    if (Effect && Resource) {

        const authResponse = {
            principalId,
            policyDocument: {
                Version: "2012-10-17",
                Statement: [
                    {
                        Effect,
                        Action: 'execute-api:Invoke',
                        Resource
                    }
                ]
            },
            context: {
                "stringKey": "stringVal",
                "numberKey": 123,
                "booleanKey": true,
                simpleAuth: true
            }
        }
        return authResponse
    } else {
        return {}
    }
}