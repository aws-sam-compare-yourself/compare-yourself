const AWS = require('aws-sdk')
const DynamoDB = new AWS.DynamoDB.DocumentClient()
const respose = require('responseHandler')

const TableName = process.env.TABLE_NAME


exports.handler = async (event) => {
    try {
        console.log({ event })

        const params = {
            Key: {
                UserId: "User0.6306792070968554"
            },
            TableName
        }

        const data = await DynamoDB.delete(params).promise()

        return respose.resposeStatus(200, {
            message: "Successfully Deleted..!",
            data: "NEvin EDwin"
        })

    } catch (err) {
        return err
    }
}